package com.jeppesen.assignment.controller.services.converter;

import java.math.BigDecimal;

/* As conversions typically happen between values of same type eg:
(currency -> currency)
(temperature -> temperature)
(measure unit -> measure unit)
It is assumed that they have same base type - represented by generic E
 */
public interface Converter<E,G> {
    E convert(E input, G outUnit);
}
