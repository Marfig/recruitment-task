package com.jeppesen.assignment.controller.services.calculator;

import com.jeppesen.assignment.controller.services.converter.Converter;
import com.jeppesen.assignment.model.Length;
import com.jeppesen.assignment.model.LengthUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;

/*
Assumes that output unit should be same as first input unit.
 */
@Service
public class BasicLengthCalculator implements BasicInterUnitCalculator<Length> {
    
    private Converter<Length, LengthUnit> converter;

    @Autowired
    public BasicLengthCalculator(Converter<Length, LengthUnit> converter) {
        this.converter = converter;
    }

    @Override
    public Length add(Length a, Length b) {
        Length convertedB = converter.convert(b,a.getUnit());
        return new Length(a.getLengthAmount().add(convertedB.getLengthAmount()),a.getUnit());
    }

    @Override
    public Length subtract(Length a, Length b) {
        Length convertedB = converter.convert(b,a.getUnit());
        return new Length(a.getLengthAmount().subtract(convertedB.getLengthAmount()),a.getUnit());
    }

    @Override
    public Length divide(Length a, Length b) {
        Length convertedB = converter.convert(b,a.getUnit());
        return new Length(a.getLengthAmount().divide(convertedB.getLengthAmount(),20, RoundingMode.HALF_UP),a.getUnit());
    }

    @Override
    public Length multiply(Length a, Length b) {
        Length convertedB = converter.convert(b,a.getUnit());
        return new Length(a.getLengthAmount().multiply(convertedB.getLengthAmount()),a.getUnit());
    }
}
