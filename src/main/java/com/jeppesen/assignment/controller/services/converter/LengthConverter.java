package com.jeppesen.assignment.controller.services.converter;

import com.jeppesen.assignment.model.Length;
import com.jeppesen.assignment.model.LengthUnit;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/*
To fulfill OCP from SOLID converter assumes that any length unit can be converted into
another unit via intermediate step of conversion to meters. Eq: Nautical Miles -> Meters -> Feet

This way we do not have to update conversion rates for old measurement unit when adding new ones.
 */
@Service
public class LengthConverter implements Converter<Length,LengthUnit> {

    @Override
    public Length convert(Length input, LengthUnit outUnit) {
        BigDecimal totalConversionFactor = input.getUnit().getToMetersConversionRatio().
                multiply(outUnit.getFromMetersConversionRatio());
        return new Length(input.getLengthAmount().multiply(totalConversionFactor),outUnit);
    }
}
