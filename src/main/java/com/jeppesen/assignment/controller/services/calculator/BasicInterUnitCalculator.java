package com.jeppesen.assignment.controller.services.calculator;

public interface BasicInterUnitCalculator<E> {

    E add(E summandA, E summandB);
    E subtract(E minuend, E subtrahend);
    E divide(E dividend, E divisor);
    E multiply(E factorA, E factorB);
}
