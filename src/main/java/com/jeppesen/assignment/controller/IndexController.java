package com.jeppesen.assignment.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/gui")
public class IndexController {

    @RequestMapping("")
    String index(){
        return "index";
    }
}
