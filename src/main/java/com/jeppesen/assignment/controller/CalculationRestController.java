package com.jeppesen.assignment.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class CalculationRestController {


    @RequestMapping
    String api(){
        return "{ \"unit\":\"meters\", \"amount\":30 }";
    }

}
