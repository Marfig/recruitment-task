package com.jeppesen.assignment.model;

import java.math.BigDecimal;

public enum LengthUnit {
    METER(BigDecimal.ONE),
    FOOT(new BigDecimal("0.3048")),
    INTERNATIONAL_NAUTICAL_MILE(new BigDecimal("1852")),
    UK_NAUTICAL_MILE(new BigDecimal("1853")); //Brits being brits


    private BigDecimal toMetersConversionRatio;
    private BigDecimal fromMetersConversionRatio;
    private static final int PRECISION = 20; //Arbitrary precision - should be enough for any reasonable unit

    LengthUnit(BigDecimal toMetersRatio){
        this.toMetersConversionRatio = toMetersRatio;
        this.fromMetersConversionRatio = BigDecimal.ONE.divide(toMetersRatio,PRECISION,BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal getToMetersConversionRatio() {
        return toMetersConversionRatio;
    }

    public BigDecimal getFromMetersConversionRatio() {
        return fromMetersConversionRatio;
    }


}
