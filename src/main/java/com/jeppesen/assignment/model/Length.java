package com.jeppesen.assignment.model;

import java.math.BigDecimal;

public class Length{

    private BigDecimal lengthAmount;
    private LengthUnit unit;

    public Length(BigDecimal lengthAmount, LengthUnit unit) {
        this.lengthAmount = lengthAmount;
        this.unit = unit;
    }

    public BigDecimal getLengthAmount() {
        return lengthAmount;
    }

    public void setLengthAmount(BigDecimal lengthAmount) {
        this.lengthAmount = lengthAmount;
    }

    public LengthUnit getUnit() {
        return unit;
    }

    public void setUnit(LengthUnit unit) {
        this.unit = unit;
    }

}
