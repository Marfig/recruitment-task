package com.jeppesen.assignment.unit;

import com.jeppesen.assignment.controller.services.calculator.BasicInterUnitCalculator;
import com.jeppesen.assignment.model.Length;
import com.jeppesen.assignment.model.LengthUnit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BasicLengthCalculatorTest {

    @Autowired
    BasicInterUnitCalculator<Length> calculator;

    @Test
    public void additionTest(){
        Length lengthA = new Length(new BigDecimal("1.0"), LengthUnit.METER);
        Length lengthB = new Length(new BigDecimal("1.0"), LengthUnit.INTERNATIONAL_NAUTICAL_MILE);
        Length sumLength = calculator.add(lengthA,lengthB);
        assertEquals(sumLength.getUnit(), LengthUnit.METER);
        assertEquals(sumLength.getLengthAmount().compareTo(new BigDecimal("1853")),0);
    }

    @Test
    public void subtractionTest(){
        Length lengthA = new Length(new BigDecimal("1.0"), LengthUnit.METER);
        Length lengthB = new Length(new BigDecimal("1.0"), LengthUnit.INTERNATIONAL_NAUTICAL_MILE);
        Length diffLength = calculator.subtract(lengthA,lengthB);
        assertEquals(diffLength.getUnit(), LengthUnit.METER);
        assertEquals(diffLength.getLengthAmount().compareTo(new BigDecimal("-1851")),0);
    }

    @Test
    public void multiplicationTest(){
        Length lengthA = new Length(new BigDecimal("1.0"), LengthUnit.METER);
        Length lengthB = new Length(new BigDecimal("1.0"), LengthUnit.INTERNATIONAL_NAUTICAL_MILE);
        Length multipliedLength = calculator.multiply(lengthA,lengthB);
        assertEquals(multipliedLength.getUnit(), LengthUnit.METER);
        assertEquals(multipliedLength.getLengthAmount().compareTo(new BigDecimal("1852")),0);
    }

    @Test
    public void divisionTest(){
        Length lengthA = new Length(new BigDecimal("1.0"), LengthUnit.METER);
        Length lengthB = new Length(new BigDecimal("1.0"), LengthUnit.INTERNATIONAL_NAUTICAL_MILE);
        Length multipliedLength = calculator.divide(lengthA,lengthB);
        assertEquals(multipliedLength.getUnit(), LengthUnit.METER);
        assertEquals(multipliedLength.getLengthAmount().setScale(12, RoundingMode.HALF_UP).compareTo(new BigDecimal("0.000539956803")),0);
    }

}
