package com.jeppesen.assignment.unit;

import com.jeppesen.assignment.controller.services.converter.Converter;
import com.jeppesen.assignment.model.Length;
import com.jeppesen.assignment.model.LengthUnit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LengthConverterTest {

    @Autowired
    Converter<Length,LengthUnit> converter;

    @Test
    public void convertMetersToFeet(){
        Length meters = new Length(new BigDecimal("1.0"),LengthUnit.METER);
        Length feet = new Length(new BigDecimal("3.28084"),LengthUnit.FOOT);
        Length converted = converter.convert(meters,LengthUnit.FOOT);
        assertEquals(feet.getLengthAmount().compareTo(converted.getLengthAmount().setScale(5, RoundingMode.HALF_UP)),0);
    }

    @Test
    public void convertMetersToInternationalNauticalMiles(){
        Length meters = new Length(new BigDecimal("1.0"),LengthUnit.METER);
        Length nMiles = new Length(new BigDecimal("0.000539956803"),LengthUnit.INTERNATIONAL_NAUTICAL_MILE);
        Length converted = converter.convert(meters,LengthUnit.INTERNATIONAL_NAUTICAL_MILE);
        assertEquals(nMiles.getLengthAmount().compareTo(converted.getLengthAmount().setScale(12, RoundingMode.HALF_UP)),0);
    }

    @Test
    public void convertMetersToUkNauticalMiles(){
        Length meters = new Length(new BigDecimal("1.0"),LengthUnit.METER);
        Length nMiles = new Length(new BigDecimal("0.0005399568"),LengthUnit.INTERNATIONAL_NAUTICAL_MILE);
        Length converted = converter.convert(meters,LengthUnit.INTERNATIONAL_NAUTICAL_MILE);
        assertEquals(nMiles.getLengthAmount().compareTo(converted.getLengthAmount().setScale(10, RoundingMode.HALF_UP)),0);
    }


    @Test
    public void convertFeetToMeters(){
        Length meters = new Length(new BigDecimal("0.3048"),LengthUnit.METER);
        Length feet = new Length(new BigDecimal("1.0"),LengthUnit.FOOT);
        Length converted = converter.convert(feet,LengthUnit.METER);
        assertEquals(meters.getLengthAmount().compareTo(converted.getLengthAmount().setScale(4, RoundingMode.HALF_UP)),0);
    }

    @Test
    public void convertFeetToInterantionalNauticalMiles(){
        Length nNmi = new Length(new BigDecimal("0.000164579"),LengthUnit.INTERNATIONAL_NAUTICAL_MILE);
        Length feet = new Length(new BigDecimal("1.0"),LengthUnit.FOOT);
        Length converted = converter.convert(feet,LengthUnit.INTERNATIONAL_NAUTICAL_MILE);
        assertEquals(nNmi.getLengthAmount().compareTo(converted.getLengthAmount().setScale(9, RoundingMode.HALF_UP)),0);
    }
}
